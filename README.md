## import

This is a script that provides a facility for importing bash "modules". It's simplistic, but gets the job done.

## Configuration and Installation

Wiki article: [configuration](https://gitlab.com/bradenbest/import/wikis/configuration)

Type 'make' and follow the instructions. Couldn't be more simple

At a minimum level, you must run `make configure` or `make build` to build it. See wiki page for more.

## Usage

Say you have a directory /opt/utils

In that directory, you have several modules. A module in this sense is the same as a module in the python sense; a collection of functions.

    . init_import /opt/utils
    import math
    import regex
    import string

It will import math, regex, and string from /opt/utils.

If you do not provide an argument to `init_import`, it will default to `DEFAULT_PATH`.

If you aren't going to pass an argument to init_import, I suggest passing `-n` instead (added in the last commit, v0.2.1)

See demo 'example' (in test/) for more

For an example with actual use, see [here](https://gitlab.com/bradenbest/import/wikis/example).

## Dependencies

This script, as of v0.2.0, will track dependencies if a module has `DEPENDS` defined. `DEPENDS` must be a list of shell commands, like so

    DEPENDS="youtube-dl curl"
    your-function(){
        ...
    }
    ...

This will make import check if they exist in `PATH`, and if they don't, it will raise a `dependency error`.

Note that you *need to* put quotes. Upon testing, I found that if a non-existing program is put into `DEPENDS`, *bash* will raise an error, rather than *import*. This was fixed by adding quotes.

Import will automatically check `<main>` for dependencies if `DEPENDS` has been set in `<main>`. To turn this off, set `MAIN_CHECK_DEPENDS` to `0` before calling `init_import`

## Conflicts

It is possible that, in importing a module, you may inadvertently overwrite an existing function. This script will automatically check for such conflicts, and will prevent a module from being imported, with an error message, should there be a conflict.

To disable conflict checking entirely, set `CHECK_CONFLICTS` to 0 before initializing 

    CHECK_CONFLICTS=0
    . init_import /opt/utils
    import math
    import regex
    import string

## Header files

If you want conflict safety with your modules, you may optionally define .h files of the same name for each module. If you have a module "math", then its header would be named "math.h".

Every line must contain a single function name from the main file. You may insert bash comments, as the import script will filter them out.

For example, say your `math` module looks like this:

    add(){
        echo $[$1 + $2]
    }
    sub(){
        echo $[$1 - $2]
    }

Your math header file (math.h) would look like this:

    add
    sub

You generally do not have to use header files, as the script should be able to identify the functions from the module. 

But, this script is, by no means, perfect. If it is, say, generating false positives, THAT is when you should use a header file.

If your scripts are going to be very complex, especially in regards to string operations/regex, then you may want to consider header files.

See demo 'conflict' for more

## Notes

* `<main>` (or "main module") refers to the script that loads init_import.
* I recommend naming your files and directories without any spaces (use `-` and `_` instead). Trust me on this one, it will make your life easier.
* I recommend against pushing the conflict checking. That means try to avoid e.g. strings with "(){" in them. I kinda need that to identify the function names.
  * If you must do something like that, then there is a header file option (see section on Header files) to override the main module
* If you must define functions in `<main>`, define them AFTER the first call to init_import, otherwise, a false positive ("self conflict") will be raised
  * Alternatively, you can set `IMPORT_FUNC_LIST` to something like `<main>::name of function` before calling `init_import`. As long as it's non-empty, the script will not scan `<main>` (which would lead to it finding the function in the file, and determining that it already exists in runtime, etc.)