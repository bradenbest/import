all:
	@echo "Enter 'make configure' to configure"
	@echo "Enter 'make build' to configure with default settings"
	@echo "Enter 'sudo make install' to install"
	@echo "Enter 'sudo make uninstall' to uninstall"

build:
	cd src && bash install -b

configure:
	cd src && bash install -c

install: src/init_import
	cd src && bash install -i

uninstall:
	cd src && bash install -u
