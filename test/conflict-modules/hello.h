# This is a header file. The idea is to protect against modules overwriting existing functions. If a header exists, the importer will check all functions in the header against the existing functions, and stop the import if there is a conflict. See 'conflict' script for a demonstration.

hello_world
hello_user
